﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    float acceleration = 0.0f;

	// Update is called once per frame
	void Update () {
        MoveCharacter();
        RotateCharacter();
    }

    //Gathers input to move the character
    void MoveCharacter()
    {
        //If the player presses W
        if (Input.GetKey(KeyCode.W))
        {
            //Increase forward acceleration
            if (acceleration < 10.0f)
            {
                acceleration += 0.1f;
            }
        }
        //If the player is not pressing forward .
        if (!Input.GetKey(KeyCode.W))
        {
            //Decrease acceleration.
            if (acceleration > 0.0f)
            {
                acceleration -= 0.1f;
            }
        }
        //Compensate for floating point imprecision.
        //If the player is not supposed to be moving, explicitly tell him so.
        if (acceleration > -0.05f && acceleration < 0.05f)
        {
            acceleration = 0.0f;
        }
        //Move the character in its own forward direction while taking acceleration and time into account.
        //The Time.deltaTime maintains consistent speed across all machines by syncing the speed with time.
        //Here is where the magic happens.
        transform.Translate(transform.forward * acceleration * Time.deltaTime, Space.World);
    }
    //Gathers input to rotate the character.
    void RotateCharacter()
    {
        //If the player presses D
        if (Input.GetKey(KeyCode.D))
        {
            //Rotate the current game object, using deltaTime for consistency across machines.
            transform.Rotate(transform.up, 100.0f * Time.deltaTime, Space.World);
        }
        //If the player presses A
        if (Input.GetKey(KeyCode.A))
        {
            //Rotate the current game object's transform, using deltaTim for consistency across machines.
            transform.Rotate(transform.up, -100.0f * Time.deltaTime, Space.World);
        }
    }
}
