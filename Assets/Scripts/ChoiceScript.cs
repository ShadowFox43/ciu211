﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChoiceScript : MonoBehaviour {

    public GameObject KillButton;
    public GameObject KnockoutButton;

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Tim")
        {
            KillButton.SetActive(true);
            KnockoutButton.SetActive(true);
            Destroy(gameObject);
        }
    }
}
