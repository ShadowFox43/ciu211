﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class BossRoomCutscene : MonoBehaviour {
    public GameObject buttonController;
    private ButtonController ButtonControllerScript;
    public int noOfKilledEnemies;
    public int noOfKnockedOutEnemes;
    public GameObject ContinueButtonAndText;

    // Use this for initialization
    void Start () {
        ButtonControllerScript = buttonController.GetComponent<ButtonController>();
        noOfKilledEnemies = ButtonControllerScript.killEnemyNo;
        noOfKnockedOutEnemes = ButtonControllerScript.KnockOutEnemyNo;
    }

    // Update is called once per frame
    void Update () {
        ButtonControllerScript = buttonController.GetComponent<ButtonController>();
        noOfKilledEnemies = ButtonControllerScript.killEnemyNo;
        noOfKnockedOutEnemes = ButtonControllerScript.KnockOutEnemyNo;
    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Tim")
        {
            if (noOfKilledEnemies == 0 && noOfKnockedOutEnemes == 0)
            {
                ContinueButtonAndText.SetActive(true);
            }
            if (noOfKnockedOutEnemes > 0 && noOfKilledEnemies < 1)
            {
                SceneManager.LoadScene(2);
            }
            if (noOfKilledEnemies > 0 && noOfKnockedOutEnemes < 1)
            {
                SceneManager.LoadScene(3);
            }
            if (noOfKilledEnemies > 0 && noOfKnockedOutEnemes > 0)
            {
                SceneManager.LoadScene(4);
            }
        }
    }
}
