﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyInfoController : MonoBehaviour {
    public GameObject KeyInfo;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.Escape))
        {
            KeyInfo.SetActive(true);
        }
        else
        {
            KeyInfo.SetActive(false);
        }

    }
}
