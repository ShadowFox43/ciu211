﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ButtonController : MonoBehaviour {

    public int killEnemyNo;
    public int KnockOutEnemyNo;

    public GameObject KillButton;
    public GameObject KnockoutButton;
    public GameObject ContinueButtonAndText;
    public GameObject LevelInfo;
    public GameObject FirstTextIntro;
    public GameObject SecondTextIntro;


    public void PlayGame()
    {
        SceneManager.LoadScene(1);
    }
    public void MainMenu()
    {
        SceneManager.LoadScene(0);
    }
    public void ViewCredits()
    {
        SceneManager.LoadScene(5);
    }
    public void KillEnemy()
    {
        killEnemyNo =+ 1;
        KillButton.SetActive(false);
        KnockoutButton.SetActive(false);
}
    public void KnockOutEnemy()
    {
        KnockOutEnemyNo =+ 1;
        KillButton.SetActive(false);
        KnockoutButton.SetActive(false);
    }
    public void Continue()
    {
        ContinueButtonAndText.SetActive(false);
    }
    public void NextTextInto()
    {
        SecondTextIntro.SetActive(true);
        FirstTextIntro.SetActive(false);
    }
    public void StartLevel()
    {
        LevelInfo.SetActive(false);
    }
    public void QuitGame()
    {
        Application.Quit();
    }

}
